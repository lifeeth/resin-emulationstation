apt-get update
apt-get install -y libudev-dev libasound2-dev libdbus-1-dev libraspberrypi0 libraspberrypi-bin libraspberrypi-dev libboost-system-dev libboost-filesystem-dev libboost-date-time-dev libfreeimage-dev libfreetype6-dev libeigen3-dev libcurl4-openssl-dev libasound2-dev cmake g++-4.7
wget http://libsdl.org/release/SDL2-2.0.1.tar.gz
tar xvfz SDL2-2.0.1.tar.gz
rm SDL2-2.0.1.tar.gz
pushd SDL2-2.0.1
./configure
make
sudo make install
popd
rm -rf SDL2-2.0.1

git clone https://github.com/Aloshi/EmulationStation
cd EmulationStation
git checkout unstable
cmake -D CMAKE_CXX_COMPILER=g++-4.7 .
make
sudo make install


